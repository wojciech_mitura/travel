window.addEventListener('load',function() {
    
    function burgerMenuHandler() {
        const burger = document.getElementById('burger');
        const burgerNav = document.getElementById('burger-nav')
        
        burger.addEventListener('click', function() {                
            burger.classList.toggle("burger-click");
            burgerNav.classList.toggle("active");
        });    
    }

    function slickSliderCustomersHandler() {
        var prevButton = document.getElementsByClassName("arrowPrev")[0];
        var nextButton = document.getElementsByClassName("arrowNext")[0];

        $('#customersOpinion').slick({
            slidesToScroll: 1,
            infinite: true,
            cssEase: 'linear',
            dots: false,
            arrows: true,
            prevArrow: prevButton,
            nextArrow: nextButton,
            autoplay: true,
            autoplaySpeed: 4000
        });
    }

    function slickSliderClientsHandler() {
        $('#contentClients').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            cssEase: 'linear',
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 1000
        });
    }

    slickSliderCustomersHandler();
    slickSliderClientsHandler()
    burgerMenuHandler();
    $('.parallax-effect').parallax();
});